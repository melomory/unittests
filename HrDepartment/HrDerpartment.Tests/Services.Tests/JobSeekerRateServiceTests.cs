using HrDepartment.Application.Interfaces;
using HrDepartment.Application.Services;
using HrDepartment.Domain.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections;
using System.Threading.Tasks;

namespace HrDerpartment.Application.UnitTests
{
    [TestFixture]
    public class JobSeekerRateServiceTests
    {
        protected JobSeekerRateService _service;

        [SetUp]
        public void Setup()
        {
            Mock<ISanctionService> mock = new();
            _service = new(mock.Object);
        }

        [Test]
        public void Constructor_SanctionServiceIsNull_ThrowsArgumentNullException()
        {           
            Assert.Throws<ArgumentNullException>(() => new JobSeekerRateService(null));
        }
        
        [Test]
        public async Task CalculateJobSeekerRatingAsync_MinumJobSeekerRating_RatingIsZero()
        {
            var jobSeeker = new JobSeeker()
            {
                BirthDate = DateTime.Now.AddYears(-17),
                Education = HrDepartment.Domain.Enums.EducationLevel.None,
                Experience = 0,
                BadHabits = HrDepartment.Domain.Enums.BadHabits.Drugs
                          | HrDepartment.Domain.Enums.BadHabits.Alcoholism
                          | HrDepartment.Domain.Enums.BadHabits.Smoking
            };
            var result = await _service.CalculateJobSeekerRatingAsync(jobSeeker);

            Assert.That(result, Is.EqualTo(0));
        }

        [Test]
        public async Task CalculateJobSeekerRatingAsync_MaximumJobSeekerRating_RatingIsOneHundred()
        {
            var jobSeeker = new JobSeeker()
            {
                BirthDate = DateTime.Now.AddYears(-18),
                Education = HrDepartment.Domain.Enums.EducationLevel.University,
                Experience = 10,
                BadHabits = HrDepartment.Domain.Enums.BadHabits.None
            };

            var result = await _service.CalculateJobSeekerRatingAsync(jobSeeker);

            Assert.That(result, Is.EqualTo(95));
        }

        [Test]
        [TestCaseSource(typeof(CalculatedJobSeekerRatingTestCasesClass), nameof(CalculatedJobSeekerRatingTestCasesClass.CalculatedJobSeekerRatingTestCases))]
        public async Task<int> CalculateJobSeekerRatingAsync_CalculatedJobSeekerRating_ReturnsRating(JobSeeker jobSeeker)
        {
            var result = await _service.CalculateJobSeekerRatingAsync(jobSeeker);

            return result;
        }

        [Test]
        public void CalculateJobSeekerRatingAsync_WrongEducationalLevel_ThrowsArgumentOutOfRangeException()
        {
            var jobSeeker = new JobSeeker()
            {
                Education = (HrDepartment.Domain.Enums.EducationLevel)15,
            };

            Assert.ThrowsAsync<ArgumentOutOfRangeException>(() => _service.CalculateJobSeekerRatingAsync(jobSeeker));
        }

        [Test]
        public async Task CalculateJobSeekerRatingAsync_JobSeekerIsInSanctionsList_ReturnsTrue()
        {
            var jobSeeker = new JobSeeker();

            Mock<ISanctionService> mock = new Mock<ISanctionService>();
            mock.Setup(
                r => r.IsInSanctionsListAsync
                (
                    It.IsAny<string>(), 
                    It.IsAny<string>(), 
                    It.IsAny<string>(), 
                    It.IsAny<DateTime>()
                )).Returns(Task.FromResult(true));

            var service = new JobSeekerRateService(mock.Object);

            var result = await service.CalculateJobSeekerRatingAsync(jobSeeker);

            Assert.That(result, Is.EqualTo(0));
        }
    }

    public class CalculatedJobSeekerRatingTestCasesClass
    {
        public static IEnumerable CalculatedJobSeekerRatingTestCases
        {
            get
            {
                yield return new TestCaseData(new JobSeeker()
                {
                    BirthDate = DateTime.Now.AddYears(-18),
                    Education = HrDepartment.Domain.Enums.EducationLevel.University,
                    Experience = 9,
                    BadHabits = HrDepartment.Domain.Enums.BadHabits.Drugs
                              | HrDepartment.Domain.Enums.BadHabits.Alcoholism
                              | HrDepartment.Domain.Enums.BadHabits.Smoking
                }).Returns(15);

                yield return new TestCaseData(new JobSeeker()
                {
                    BirthDate = DateTime.Now.AddYears(-18),
                    Education = HrDepartment.Domain.Enums.EducationLevel.College,
                    Experience = 4,
                    BadHabits = HrDepartment.Domain.Enums.BadHabits.Alcoholism
                              | HrDepartment.Domain.Enums.BadHabits.Smoking
                }).Returns(30);

                yield return new TestCaseData(new JobSeeker()
                {
                    BirthDate = DateTime.Now.AddYears(-18),
                    Education = HrDepartment.Domain.Enums.EducationLevel.School,
                    Experience = 2,
                    BadHabits = HrDepartment.Domain.Enums.BadHabits.Alcoholism
                              | HrDepartment.Domain.Enums.BadHabits.Smoking
                }).Returns(15);
            }
        }
    }
}